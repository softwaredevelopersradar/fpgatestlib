﻿namespace TestHardwareLib.Data
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;

    using Microsoft.VisualBasic;

    public struct FrequencyRange : IEquatable<FrequencyRange>
    {
        public float StartFrequencyKhz { get; }
        public float EndFrequencyKhz { get; }

        public float CentralFrequencyKhz => (StartFrequencyKhz + EndFrequencyKhz) / 2;

        public float RangeSizeKhz => EndFrequencyKhz - StartFrequencyKhz;

        public FrequencyRange(float startFrequencyKhz, float endFrequencyKhz) : this()
        {
            Contract.Assert(startFrequencyKhz > 0);
            Contract.Assert(startFrequencyKhz <= endFrequencyKhz);

            StartFrequencyKhz = startFrequencyKhz;
            EndFrequencyKhz = endFrequencyKhz;
        }

        public bool Contains(float frequencyKhz)
        {
            return StartFrequencyKhz <= frequencyKhz && frequencyKhz <= EndFrequencyKhz;
        }


        public static bool AreIntersected(FrequencyRange range1, FrequencyRange range2)
        {
            if (range1.StartFrequencyKhz > range2.StartFrequencyKhz)
            {
                return range2.StartFrequencyKhz <= range1.EndFrequencyKhz &&
                       range1.StartFrequencyKhz <= range2.EndFrequencyKhz;
            }
            return range1.StartFrequencyKhz <= range2.EndFrequencyKhz &&
                   range2.StartFrequencyKhz <= range1.EndFrequencyKhz;
        }

        public bool Equals(FrequencyRange other)
        {
            return Math.Abs(StartFrequencyKhz - other.StartFrequencyKhz) < 1
                && Math.Abs(EndFrequencyKhz - other.EndFrequencyKhz) < 1;
        }

        /// <summary>
        /// Factor of ranges intersection:
        /// 0 - ranges are not intesected,
        /// 0.5 - ranges are partly intersected, 
        /// 1 - one of the ranges is fully lying in another, and so on.
        /// </summary>
        public static float IntersectionFactor(FrequencyRange range1, FrequencyRange range2)
        {
            var minRangeSize = Math.Min(range1.RangeSizeKhz, range2.RangeSizeKhz);
            var intersection = Intersection(range1, range2);
            if (intersection == null)
            {
                return 0;
            }
            if (Math.Abs(minRangeSize) < 1e-3f)
            {
                return 1;
            }

            return intersection.Value.RangeSizeKhz / minRangeSize;
        }

        /// <summary>
        /// Union of two intersected ranges
        /// </summary>
        public static FrequencyRange Union(FrequencyRange range1, FrequencyRange range2)
        {
            var minX = Math.Min(range1.StartFrequencyKhz, range2.StartFrequencyKhz);
            var maxX = Math.Max(range1.EndFrequencyKhz, range2.EndFrequencyKhz);
            return new FrequencyRange(minX, maxX);
        }

        public static FrequencyRange? Intersection(FrequencyRange range1, FrequencyRange range2)
        {
            if (!AreIntersected(range1, range2))
            {
                return null;
            }
            if (range1.StartFrequencyKhz <= range2.StartFrequencyKhz)
            {
                if (range2.EndFrequencyKhz <= range1.EndFrequencyKhz)
                {
                    // range2 is fully lies in the range1
                    return range2;
                }
                return new FrequencyRange(range2.StartFrequencyKhz, range1.EndFrequencyKhz);
            }
            else
            {
                if (range1.EndFrequencyKhz <= range2.EndFrequencyKhz)
                {
                    // range1 is fully lies in the range2
                    return range1;
                }
                return new FrequencyRange(range1.StartFrequencyKhz, range2.EndFrequencyKhz);

            }
        }

        public override string ToString()
        {
            return $"Frequency range: from {StartFrequencyKhz} KHz, to {EndFrequencyKhz} Khz";
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is FrequencyRange range && Equals(range);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (StartFrequencyKhz.GetHashCode() * 397) ^ EndFrequencyKhz.GetHashCode();
            }
        }
    }

}