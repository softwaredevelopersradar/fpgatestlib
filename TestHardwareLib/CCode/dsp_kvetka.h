#ifndef __SVP713_DSP_H__
#define __SVP713_DSP_H__

// #define BAR0 0x7F000000
// #define BAR1 0xC0000000
// #define BAR2 0x7E000000

#define BAR0 0
#define BAR1 1
#define BAR2 2

// offsets: FS_
#define FS_CTRL_FPGA 0x20008

#define FS_CTRL_TDSP 0x10044

#endif
