#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>

#include "devapi.h"
#include "utill.h"
#include "dsp_kvetka.h"

#define DEVICE_NUM 0

typedef int DEVICE_HANDLE;

void sleep();
void sleep_var(int iter);

void sleep() {
	unsigned int timer;

	for (timer = 0; timer < TIMER_BOARD; timer++) {}
}

void sleep_var(int iter) {
	unsigned int loop, iter_internal, timer;

	printf("wait");

	if (iter <= 0) {
		iter_internal = 1;
	} else {
		iter_internal = iter;
	}

	for (loop = 0; loop < iter_internal; loop++) {
		for (timer = 0; timer < TIMER_BOARD; timer++) {}
			printf(".");
	}

	printf("\n");
}

int main(int argc, char **argv)
{
    int i, currerr, retv1, retv2, retv3;
	unsigned int timer;

	u_int32_t val1, val2, val3;
	DEVICE_HANDLE hdl;
	DEVINFO di;
	
	currerr = 0;
	val1 = 0;
	val2 = 0;
	val3 = 0;
	
	retv1 = 0;
	retv2 = 0;
	retv3 = 0;

	hdl = devOpen(DEVICE_NUM);
	if (hdl < 0)
	{
		currerr = 1;
		printf("SVP-713 #%u: Can't open device!\n", DEVICE_NUM);
		goto Error;
	}

	if (get_dev_info(hdl, &di) == -1)
	{
		currerr = 1;
		printf("SVP-713 #%u: Can't get device information!\n", DEVICE_NUM);
		goto CloseDev;
	}

	printf("Device info: %x\n", di);

	// --------------------------------------------------------

	// set point size
	dev_write_ulong(hdl, BAR0, FS_CTRL_TDSP, 0x00000004);
	printf("set point size: done.\n");	
	sleep();

	// write threshold
	dev_write_ulong(hdl, BAR0, FS_CTRL_TDSP, 0x00000010);
	printf("write threshold: done.\n");
	sleep();

	// net init
	dev_write_ulong(hdl, BAR0, FS_CTRL_TDSP, 0x00000012);
	printf("net init: done.\n");
	sleep_var(15);

	// enable PPRCH
	retv1 = dev_read_ulong(hdl, BAR2, FS_CTRL_FPGA, &val1);
	val1 = val1|0x20000000; // 29 bit > 1
	dev_write_ulong(hdl, BAR2, FS_CTRL_FPGA, val1);
	printf("set PPRCH mode: done.\n");
	sleep();

	// --------------------------------------------------------

	CloseDev:	
		if (devClose(hdl) < 0)
		{
			if (currerr == 0) printf("SVP-713 #%u: Can't close device!\n", DEVICE_NUM);
			currerr = 1;
		}

	Error:
		if (currerr == 0) printf("Done. Stop svp connect.\n");
		else printf("SVP-713 #%u: test FAILED!\n", DEVICE_NUM);

	return 0;
}

