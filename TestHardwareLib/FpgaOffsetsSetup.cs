﻿namespace TestHardwareLib
{
    public class FpgaOffsetsSetup
    {
        public byte CmdTblBar = 0;
        public byte AxiCdmaBar;
        public byte AxiPcie0Bar;
        public byte AxiPcie1Bar;
        public byte AxiPcieBar;
        public byte Adc1Bar;
        public byte Adc2Bar;
        public uint CmdTblOff = 0x00010000;
        public uint AxiCdmaOff;
        public uint AxiPcie0Off;
        public uint AxiPcie1Off;
        public uint AxiPcieOff;
        public uint Adc1Base;
        public uint Adc2Base;
        public uint Adc1Off;
        public uint Adc2Off;
        public uint AxiBar0D0Off;
        public uint AxiBar0D1Off;
        public uint AxiBar0D0Size;
        public uint AxiBar0D1Size;
        public uint AxiBarOff;
        public uint AxiBarSize;

        public FpgaOffsetsSetup()
        {
            this.AxiCdmaBar = this.AxiPcie0Bar = this.AxiPcie1Bar = this.AxiPcieBar = this.Adc1Bar = this.Adc2Bar = 0xFF;
            this.AxiCdmaOff = this.AxiPcie0Off = this.AxiPcie1Off = this.AxiPcieOff = this.Adc1Base = this.Adc2Base = this.Adc1Off = this.Adc2Off =
                                                                        this.AxiBar0D0Off = this.AxiBar0D1Off = this.AxiBar0D0Size = this.AxiBar0D1Size = this.AxiBarOff = this.AxiBarSize = 0xFFFFFFFF;
        }
    }
}