﻿namespace TestHardwareLib.HardwareData
{
    public enum Endianness
    {
        Unset = 0,
        Unknown = 1,
        Le = 2,
        Be = 3
    }
}