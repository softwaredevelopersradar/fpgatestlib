﻿namespace TestHardwareLib.HardwareData
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi, Size = 64)]
    public struct DmaConfigin
    {
        public CmdStatus Status;
        public byte DmaEngType;
        public byte WorkMode;
        public byte OpCode;
        public uint EngOps;
        public byte PCIBar;
        public uint PCIOff;
        public byte DMABar;
        public uint DMAOff;
        public uint AxiBarSize;
        public uint TDListAddr;
        public byte TDListBar;
        public uint TDListOff;
        public uint TDListMaxSize;
        public byte DmaBufNumber;
        public uint DmaBufAlignment;
        public uint DmaSimpleTransferMaxSize;
        public uint DmaSGTransferMaxSize;
        public byte BarForDataBuf;
        public uint FromAddr;
        public uint ToAddr;
        public uint Length;
    }
}