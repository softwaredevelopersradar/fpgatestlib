﻿namespace TestHardwareLib.HardwareData
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public struct PciConfig
    {
        public byte RevisionID;
        public ushort SubVendorID;
        public ushort SubSystemID;
    }
}