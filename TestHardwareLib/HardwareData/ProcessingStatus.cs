﻿namespace TestHardwareLib.HardwareData
{
    public enum ProcessingStatus : uint
    {
        NotFinished = 0,
        FftReady = 1,
        Accepted = 2,
        Finished = 0x80000000
    }
}