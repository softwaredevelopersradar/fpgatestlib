﻿namespace TestHardwareLib.HardwareData
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi, Size = 9)]
    public struct LocalDeviceIo
    {
        public byte Bar;
        public uint Offset;
        public uint Value;
    }
}