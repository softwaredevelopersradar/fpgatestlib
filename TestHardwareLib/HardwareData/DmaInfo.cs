﻿namespace TestHardwareLib.HardwareData
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi, Size = 33)]
    public struct DmaInfo
    {
        public byte DmaEngTypeMax;
        public uint DmaBufAlignment;
        public uint DmaBufSize;
        public ulong DmaBufBaseLA;
        public ulong DevWindowBaseLA;
        public ulong InDevWindowOffsetLA;
    }
}