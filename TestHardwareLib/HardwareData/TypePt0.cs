﻿namespace TestHardwareLib.HardwareData
{
    public enum TypePt0
    {
        Notype,
        NpiStub = 0x0001,
        FreqMet,
        InterfaceTbl,
        AxiPcieCtrl,
        AxiCdma,
        Adc5G,
        Ad9129,
        Afe7225,
        Adc2A1G8,
        Ad9467,
        Dac5681Z,
        Sysmon,
        SfmGpio,
        Ads5400,
        LedGpio,
        MainFpgaRegs,
        CarierIfTbl,
        AxiDma,
        DmaBdBuf,
        AxisChecker,
        AxiI2C,
        AxiEmc,
        Fmc_2A2DSubsys,
        Fmc_4A25Subsys,
        Fmc_4D10Subsys,
        Hwicap,
        BufioCtrl,
        SwGpio
    }
}