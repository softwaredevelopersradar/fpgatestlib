﻿namespace TestHardwareLib.HardwareData
{
    public enum OpCode
    {
        ReadBRAM,
        ReadDDR,
        WriteDDR
    }
}