﻿namespace TestHardwareLib.HardwareData
{
    public enum DevFamily
    {
        Unknown = 0,
        Amc = 1,
        Vpx = 2,
        Cpci = 3,
        Comexpress = 4,
        Vpx2Fp1 = 0x102,
        Vpx2Fp2 = 0x112
    }
}