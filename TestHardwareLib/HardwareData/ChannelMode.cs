﻿namespace TestHardwareLib.HardwareData
{
    public enum ChannelMode
    {
        Intelligence = 0, Fhss = 1, FhssDurationMeasurement = 2
    }
}