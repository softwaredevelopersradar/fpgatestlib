﻿namespace TestHardwareLib.HardwareData
{
    public enum DeviceType
    {
        No = 0,
        Xdsp = 1,
        Xpci = 2,
        Setpci = 3,
        Unknown = 4,
        Samc7134A250 = 0x1000,
        Svp7134A250 = 0x4A25,
        Svp7138A250 = 0x510C,
        Svp7131A2500 = 0x1001,
        Svp7131A5000 = 0x1A50,
        Xdsp5Mckm = 0x5AAC,
        Svp7134D250 = 0x4D25,
        Sfm4D1000 = 0x4D10,
        Sfm2A1800 = 0x2A18,
        Svp7132A10002D1000 = 0x2A2D,
        Sfm2A2502D1000 = 0x2A25,
        Samc706 = 0xA706,
        Fiocoms6 = 0x410C,
        Fmc30Rf = 0xF30F,
        Sfm2D2800 = 0x1D50,
        Vc707 = 0xE707,
        Gsd = 0x1111
    }
}