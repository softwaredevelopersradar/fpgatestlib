﻿namespace TestHardwareLib.HardwareData
{
    public enum DmaEngType
    {
        No = 0,
        Xpscdma,
        AxicdmaSimple,
        Axicdmasg
    }
}