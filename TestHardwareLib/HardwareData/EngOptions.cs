﻿namespace TestHardwareLib.HardwareData
{
    public enum EngOptions
    {
        DataBufPcMem = 0x1,
        DataBufModBar = 0x2,
        AddrAutoInc = 0x4,
        AddrWrapAround = 0x8
    }
}