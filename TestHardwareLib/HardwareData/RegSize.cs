﻿namespace TestHardwareLib.HardwareData
{
    public enum RegSize
    {
        RsUnknown = 0,
        Rs8B = 1,
        Rs16B = 2,
        Rs32B = 4
    }
}