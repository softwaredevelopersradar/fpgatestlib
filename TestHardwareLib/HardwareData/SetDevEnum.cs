﻿namespace TestHardwareLib.HardwareData
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Size = 29)]
    public struct SetDevEnum
    {
        public uint DevFam;
        public uint DevType;
        public uint DevNum;
        public uint GA;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 13)]
        public string SN;
    }

    // команды для смены режимов платы ЦОС
}
