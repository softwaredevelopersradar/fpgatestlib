﻿namespace TestHardwareLib
{
    using System.Runtime.InteropServices;

    using TestHardwareLib.HardwareData;

    public unsafe class DevApiLinux
    {
        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern uint dev_read_ulong(int dev, uint bar, uint offset, out uint val);

        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern uint dev_write_ulong(int dev, uint bar, uint offset, uint val);

        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)] 
        public static extern int get_pci_config(int dev, out PciConfig ppc);

        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int get_dev_info(int dev, out DeviceType dpi);

        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern void fill_buf_w_counter(uint* start_val, uint* buf, uint len);

        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int read_data_from_dev(int dev, uint bar, uint offset, uint buf_size, uint[] buffer);
        
        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int write_data_to_dev(int dev, uint bar, uint offset, uint buf_size, uint[] buffer);

        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int save_data_to_file([MarshalAs(UnmanagedType.LPStr)] string fileName, out string buf, uint buf_size);

        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int devOpen(uint devNum);

        [DllImport("devapi.co", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int devClose(int dev);

    }
}