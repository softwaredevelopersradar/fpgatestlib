﻿using System;

namespace TestHardwareLib
{
    using System.Collections.Generic;

    using NLog;

    using TestHardwareLib.Data;
    using TestHardwareLib.Enum;
    using TestHardwareLib.HardwareData;
    using TestHardwareLib.JammingData;

    class Program
    {
        private static bool runForever = true;

        private static ILogger logger = LogManager.GetCurrentClassLogger();
        private static FpgaOffsetsSetup offsetsSetup = new FpgaOffsetsSetup();

        private static FpgaDevice device = new FpgaDevice();

        static void Main(string[] args)
        {
            InitLogger();
            device.resolution = FftResolution.N4096;
            var mask = device.GetMask(
                new RadioJamFhssTarget(
                    1500,
                    3000,
                    90,
                    1,
                    1,
                    1,
                    new List<FrequencyRange>() { new FrequencyRange(1500, 1520) },
                    1));
            while (runForever)
            {
                string userInput = InputString("Command [? for help]:", null, false);
                switch (userInput)
                {
                    case "?":
                        Menu();
                        break;
                    case "q":
                    case "Q":
                        runForever = false;
                        break;
                    case "c":
                    case "C":
                    case "cls":
                        Console.Clear();
                        break;
                    case "dru":
                        ReadLong();
                        break;
                    case "dwu":
                        WriteLong();
                        break;
                    case "gpc":
                        GetPciInfo();
                        break;
                    case "gdi":
                        GetDevInfo();
                        break;
                    case "fbwc":
                        FillBufferCounter();
                        break;
                    case "rdfd":
                        ReadDev();
                        break;
                    case "wdfd":
                        WriteDev();
                        break;
                    case "save":
                        SaveFile();
                        break;
                    case "open":
                        OpenDevice();
                        break;
                    case "netInit":
                        NetInit();
                        break;
                    case "fhssInit":
                        fhssInit();
                        break;
                    case "close":
                        CloseDevice();
                        break;
                }
            }
        }

        private static void InitLogger()
        {
        }

        private static void NetInit()
        {
            try
            {
                device.InitiateNetwork();
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
        }

        private static void fhssInit()
        {
            try
            {
                int fftCode = InputInteger(" N16384 = 3,N8192 = 4,N4096 = 5", 5, true, false);
                byte threshold = (byte)InputInteger("Threshold", 80, true, true);
                device.SetFpgaSetup((FftResolution)fftCode, threshold);
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
        }

        private static void ReadLong()
        {
            try
            {
                int devNumber = InputInteger("Device number", 0, true, true);
                uint bar = (uint)InputInteger("Bar value", offsetsSetup.CmdTblBar, true, true);
                uint offset = (uint)InputInteger("Offset value", (int)offsetsSetup.CmdTblOff, true, true);
                uint value;
                var result = DevApiLinux.dev_read_ulong(devNumber, bar, offset, out value);
                logger.Info(string.Format("dev_read_ulong result:{0}", result));
                logger.Info(string.Format("Read value:{0}", value));

            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error while writing to dev!{0}", e.Message));
            }
        }

        private static void WriteLong()
        {
            try
            {
                int devNumber = InputInteger("Device number", 0, true, true);
                uint bar = (uint)InputInteger("Bar value", offsetsSetup.CmdTblBar, true, true);
                uint offset = (uint)InputInteger("Offset value", (int)offsetsSetup.CmdTblOff, true, true);
                uint value = (uint)InputInteger("Value to write", 0, true, true);
                var result = DevApiLinux.dev_write_ulong(devNumber,bar,offset, value);
                logger.Info(string.Format("dev_write_ulong result:{0}", result));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error while writing to dev!{0}", e.Message));
            }
        }

        private static void GetPciInfo()
        {
            try
            {
                int data = InputInteger("Device number", 0, true, true);
                var result = DevApiLinux.get_pci_config(data, out var type);
                logger.Info(string.Format("get_pci_config:{0}", result));
                logger.Info(string.Format("Config:{0} {1} {2}", type.RevisionID, type.SubSystemID, type.SubVendorID));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error while getting info. {0}", e.Message));
            }
        }

        private static void GetDevInfo()
        {
            try
            {
                DeviceType type;
                int data = InputInteger("Device number", 0, true, true);
                var result = DevApiLinux.get_dev_info(data, out type);
                logger.Info(string.Format("get_dev_info result:{0}", result));
                logger.Info(string.Format("Info:{0}", type));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error while getting info. {0}", e.Message));
            }
        }

        private static void FillBufferCounter()
        {
            try
            {
                logger.Warn("I don't know what to do here!");
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        private static void ReadDev()
        {
            try
            {
                int devNumber = InputInteger("InputDevNum", 0, true, true);
                uint bar = (uint)InputInteger("Bar value", offsetsSetup.CmdTblBar, true, true);
                uint offset = (uint)InputInteger("Offset value", (int)offsetsSetup.CmdTblOff, true, true);
                uint bufferSize = (uint)InputInteger("Buffer size value", 32, true, false);
                uint[] buffer = new uint[bufferSize];
                var result = DevApiLinux.read_data_from_dev(devNumber, bar, offset, bufferSize, buffer);
                logger.Info(string.Format("read_data_from_dev result:{0}", result));
                logger.Info(string.Format("buffer values:[{0}]", string.Join(",", buffer)));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error while reading from dev!{0}", e.Message));
            }
        }

        private static void WriteDev()
        {
            try
            {
                int devNumber = InputInteger("InputDevNum", 0, true, true);
                uint bar = (uint)InputInteger("Bar value", offsetsSetup.CmdTblBar, true, true);
                uint offset = (uint)InputInteger("Offset value", (int)offsetsSetup.CmdTblOff, true, true);
                uint bufferSize = (uint)InputInteger("Buffer size value", 32, true, false);
                uint[] buffer = new uint[bufferSize];
                var result = DevApiLinux.write_data_to_dev(devNumber, bar, offset, bufferSize, buffer);
                logger.Info(string.Format("write_data_to_dev result:{0}", result));
                logger.Info(string.Format("buffer values:[{0}]", string.Join(",", buffer)));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error while writing to dev!{0}", e.Message));
            }
        }

        private static void SaveFile()
        {
            try
            {
                string fileName = InputString("FileName", "test.txt", false);
                string buffer;
                int bufferSize = InputInteger("BufferLength:", 10, true, false);
                var result = DevApiLinux.save_data_to_file(fileName,out buffer, (uint)bufferSize);
                logger.Info(string.Format("saveDataToFile result:{0}", result));
                logger.Info(string.Format("buffer value:{0}", buffer));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error while trying to save file {0}", e.Message));
            }
        }

        private static void CloseDevice()
        {
            try
            {
                int data = InputInteger("Device number", 0, true, true);
                var result = DevApiLinux.devClose(data);
                logger.Info(string.Format("devClose result:{0}", result));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error while trying to close  device {0}", e.Message));
            }
        }

        private static void OpenDevice()
        {
            try
            {
                int data = InputInteger("Device number:", 0, true, true);
                var result = device.InitiateDevice((uint)data);
                logger.Info(string.Format("devOpen result:{0}", result));
            }
            catch (Exception e)
            {
                logger.Error(string.Format("Error while trying to open device {0}", e.Message));
            }
        }

        static void Menu()
        {
            logger.Info("Available commands:");
            logger.Info(" ?             Help, this menu");
            logger.Info(" q             Quit");
            logger.Info(" cls           Clear the screen");
            logger.Info("dru  dev_read_ulong");
            logger.Info("dwu  dev_write_ulong");
            logger.Info("gpc  get_pci_config");
            logger.Info("gdi  get_dev_info");
            logger.Info("fbwc  fill_buf_w_counter");
            logger.Info("rdfd  read_data_from_dev");
            logger.Info("wdfd  write_data_to_dev");
            logger.Info("save  save_data_to_file");
            logger.Info("open  devOpen");
            logger.Info("close  devClose");
            logger.Info("netInit initiate network");
            logger.Info("fhssInit Initiate Fhss.");
        }


        static string InputString(string question, string defaultAnswer, bool allowNull)
        {
            while (true)
            {
                Console.Write(question);

                if (!String.IsNullOrEmpty(defaultAnswer))
                {
                    Console.Write(" [" + defaultAnswer + "]");
                }

                Console.Write(" ");

                string userInput = Console.ReadLine();

                if (String.IsNullOrEmpty(userInput))
                {
                    if (!String.IsNullOrEmpty(defaultAnswer)) return defaultAnswer;
                    if (allowNull) return null;
                    else continue;
                }

                return userInput;
            }
        }

        static int InputInteger(string question, int defaultAnswer, bool positiveOnly, bool allowZero)
        {
            while (true)
            {
                Console.Write(question);
                Console.Write(" [" + defaultAnswer + "] ");

                string userInput = Console.ReadLine();

                if (String.IsNullOrEmpty(userInput))
                {
                    return defaultAnswer;
                }

                int ret = 0;
                if (!Int32.TryParse(userInput, out ret))
                {
                    logger.Warn("Please enter a valid integer.");
                    continue;
                }

                if (ret == 0)
                {
                    if (allowZero)
                    {
                        return 0;
                    }
                }

                if (ret < 0)
                {
                    if (positiveOnly)
                    {
                        logger.Warn("Please enter a value greater than zero.");
                        continue;
                    }
                }

                return ret;
            }
        }
    }
}
