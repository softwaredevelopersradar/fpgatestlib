﻿namespace TestHardwareLib
{
    public static class Utilities
    {
        /// <summary>
        /// Digital arone band
        /// </summary>
        public static int BandwidthKhz = 10000;

        /// <summary>
        /// Real arone band
        /// </summary>
        public static int ReceiverBandwidthKhz = 12500;

        public static int CenterPeakSampleWidth = 21;

        public static int GetBandSamplesCount(int receiverSamplesCount)
        {
            return (BandwidthKhz * (receiverSamplesCount - 1) - receiverSamplesCount + 1) / ReceiverBandwidthKhz + 2;
        }

        public static int GetSamplesCount(float startFrequencyKhz, float endFrequencyKhz, float samplesGapKhz = 8192)
        {
            return (int)((endFrequencyKhz - startFrequencyKhz + samplesGapKhz - 1) / samplesGapKhz);
        }
    }
}