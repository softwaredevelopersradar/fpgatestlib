﻿namespace TestHardwareLib.Enum
{
    public enum Emitted
    {
        No = 0,
        Low,
        Medium,
        Powerful
    }
}