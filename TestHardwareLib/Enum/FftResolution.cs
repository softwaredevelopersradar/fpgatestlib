﻿namespace TestHardwareLib.Enum
{
    public enum FftResolution : uint
    {
        N16384 = 3,
        N8192 = 4,
        N4096 = 5,
    }
}