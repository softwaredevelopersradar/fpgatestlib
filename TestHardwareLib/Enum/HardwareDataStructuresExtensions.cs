﻿namespace TestHardwareLib.Enum
{
    using System;

    public static class HardwareDataStructuresExtensions
    {
        public static int GetSampleCount(this FftResolution self)
        {
            switch (self)
            {
                case FftResolution.N16384:
                    return 16384;
                case FftResolution.N8192:
                    return 8192;
                case FftResolution.N4096:
                    return 4096;
                default:
                    throw new ArgumentException($"Unknown FftPointsCount value: {self}");
            }
        }
    }
}