﻿namespace TestHardwareLib.Enum
{
    public enum TargetState
    {
        NotActive = 0,
        Active = 1,
        ActiveLongTime = 2,
        NotActiveLongTime = 3
    }
}