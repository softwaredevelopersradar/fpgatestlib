﻿namespace TestHardwareLib.Enum
{
    public enum TargetCreator
    {
        Unknown = 0,
        Button = 1,
        OtherTable = 2,
        PC = 3,
        RadioReceiver = 4,
        Panorama = 5
    }
}