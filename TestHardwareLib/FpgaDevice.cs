﻿namespace TestHardwareLib
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;


    using NLog;

    using TestHardwareLib.Data;
    using TestHardwareLib.Enum;
    using TestHardwareLib.JammingData;

    public class FpgaDevice
    {
        private static FpgaOffsetsSetup offsetsSetup = new FpgaOffsetsSetup();

        public FftResolution resolution;
        private ILogger logger = LogManager.GetCurrentClassLogger();
        private int DeviceId = 0;

        public int InitiateDevice(uint deviceNum)
        {
            var result = DevApiLinux.devOpen(deviceNum);
            this.DeviceId = result;
            this.logger.Info(string.Format("Device id:{0}", result));
            return result;
        }

        public bool InitiateNetwork()
        {
            // net init
            var result = DevApiLinux.dev_write_ulong(this.DeviceId, offsetsSetup.CmdTblBar, 0x10044, 0x00000012);
            if (result != 0)
            {
                this.logger.Error("Error while initializing network!");
            }
            Thread.Sleep(1000);
            return result == 0;
        }

        public void SetFpgaSetup(FftResolution resolution, byte threshold)
        {

            // set point size
            var setResolutionResult = DevApiLinux.dev_write_ulong(this.DeviceId, offsetsSetup.CmdTblBar, 0x10044, (uint)resolution);
            if (setResolutionResult != 0)
            {
                this.logger.Error(
                    string.Format(
                        "Error while setting fft resolution bar:{0} offset: {1}",
                        offsetsSetup.CmdTblBar,
                        0x10044));
            }

            this.resolution = resolution;

            //set threshold value
            var setThresholdResult = DevApiLinux.dev_write_ulong(
                this.DeviceId,
                offsetsSetup.CmdTblBar,
                0x10058,
                threshold);

            if (setThresholdResult != 0)
            {
                this.logger.Error(
                    string.Format(
                        "Error while setting threshold value in bar:{0} offset:{1}",
                        offsetsSetup.CmdTblBar,
                        0x10058));
            }

            // write threshold
            var writeThresholdResult = DevApiLinux.dev_write_ulong(this.DeviceId, offsetsSetup.CmdTblBar, 0x10044, 0x00000010);
            if (writeThresholdResult != 0)
            {
                this.logger.Error(
                    string.Format(
                        "Error while setting threshold value in bar:{0} offset:{1}",
                        offsetsSetup.CmdTblBar,
                        0x10044));
            }

            // set mask
            var mask = this.GetMask(
                new RadioJamFhssTarget(
                    1500,
                    1550,
                    threshold,
                    1,
                    1,
                    1,
                    new List<FrequencyRange>() { new FrequencyRange(1500, 1520) },
                    1));
            var writeSetMaskResult = DevApiLinux.write_data_to_dev(
                this.DeviceId,
                offsetsSetup.CmdTblBar,
                0x50000,
                (uint)mask.Length,
                mask);
            if (writeSetMaskResult != 0)
            {
                this.logger.Error(
                    string.Format("Error while setting mask in bar {0} offset:{1}", offsetsSetup.CmdTblBar, 0x50000));
            }
            // write mask

            var writeMaskResult = DevApiLinux.dev_write_ulong(
                this.DeviceId,
                offsetsSetup.CmdTblBar,
                0x10044,
                0x0000000E);
            if (writeMaskResult != 0)
            {
                this.logger.Error(
                    string.Format(
                        "Error while setting mask in bar {0} offset:{1} value:{2}",
                        offsetsSetup.CmdTblBar,
                        0x10044,
                        0x0000000E));
            }

            var setFhssMode = DevApiLinux.dev_write_ulong(this.DeviceId, 0, 0x1004C, 1);
            if (setFhssMode != 0)
            {
                this.logger.Error(
                    string.Format(
                        "Error while setting mask in bar {0} offset:{1} value:{2}",
                        offsetsSetup.CmdTblBar,
                        0x1004C,
                        1));
            }
        }

        public const int CenterPeakSampleWidth = 21;

        public uint[] GetMask(IRadioJamFhssTarget targets)
        {
            var target = targets;

            int resultArrayLength = this.resolution.GetSampleCount();
            var bandPointCount = Utilities.GetBandSamplesCount(resultArrayLength);
            var arrayOffset = (resultArrayLength - bandPointCount) / 2;
            var arrayMaxVisibleIndex = arrayOffset + bandPointCount;
            var pointsGapKhz = 1f * Utilities.ReceiverBandwidthKhz / (resultArrayLength - 1);


            var startFrequencyKhz = target.MinFrequencyKhz;
            var endFrequencyKhz = startFrequencyKhz + Utilities.BandwidthKhz;

            var lastNetworkSampleIndex = arrayOffset + GetSampleNumber(startFrequencyKhz, target.MaxFrequencyKhz);
            if (lastNetworkSampleIndex > arrayMaxVisibleIndex)
            {
                lastNetworkSampleIndex = arrayMaxVisibleIndex;
            }

            var mask = new BitArray(resultArrayLength, defaultValue: false);

            for (var i = arrayOffset; i < lastNetworkSampleIndex; ++i)
            {
                mask[i] = true;
            }

            // TODO:  remove it when Petya adds hardware realization
            var centerStartIndex = resultArrayLength / 2 - Utilities.CenterPeakSampleWidth / 2;
            var centerEndIndex = resultArrayLength / 2 + Utilities.CenterPeakSampleWidth / 2;
            for (var i = centerStartIndex; i < centerEndIndex; ++i)
            {
                mask[i] = false;
            }

            foreach (var range in target.ForbiddenRanges)
            {
                // whole band is forbidden
                if (range.StartFrequencyKhz < startFrequencyKhz && endFrequencyKhz < range.EndFrequencyKhz)
                {
                    return new uint[resultArrayLength];
                }
                if (range.StartFrequencyKhz >= startFrequencyKhz && endFrequencyKhz < range.EndFrequencyKhz)
                {
                    var offset = arrayOffset + GetSampleNumber(startFrequencyKhz, range.StartFrequencyKhz);
                    for (var i = offset; i < arrayMaxVisibleIndex; ++i)
                    {
                        mask[i] = false;
                    }
                }
                else if (range.StartFrequencyKhz < startFrequencyKhz && endFrequencyKhz >= range.EndFrequencyKhz)
                {
                    var endOffset = arrayOffset + GetSampleNumber(startFrequencyKhz, range.EndFrequencyKhz);
                    for (var i = arrayOffset; i <= endOffset; ++i)
                    {
                        mask[i] = false;
                    }
                }
                else
                {
                    var startOffset = arrayOffset + GetSampleNumber(startFrequencyKhz, range.StartFrequencyKhz);
                    var endOffset = arrayOffset + GetSampleNumber(startFrequencyKhz, range.EndFrequencyKhz);
                    for (var i = startOffset; i <= endOffset; ++i)
                    {
                        mask[i] = false;
                    }
                }
            }

            SwapMaskParts();

            var result = new uint[resultArrayLength];
            mask.CopyTo(result, 0);
            return result;

            void ReverseMask()
            {
                var length = mask.Count - 1;
                var to = mask.Count / 2;
                for (var i = 0; i < to; ++i)
                {
                    var elementI = mask[i];
                    mask[i] = mask[length - i];
                    mask[length - i] = elementI;
                }
            }

            void SwapMaskParts()
            {
                var to = mask.Count / 2;
                for (var i = 0; i < to; ++i)
                {
                    var elementI = mask[i];
                    mask[i] = mask[to + i];
                    mask[to + i] = elementI;
                }
            }

            int GetSampleNumber(float networkStartFrequencyKhz, float frequencyKhz)
            {
                return Utilities.GetSamplesCount(networkStartFrequencyKhz, frequencyKhz, pointsGapKhz);
            }
        }
    }
}